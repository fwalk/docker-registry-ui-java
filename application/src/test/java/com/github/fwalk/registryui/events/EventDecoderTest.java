package com.github.fwalk.registryui.events;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class EventDecoderTest {

    @Test
    public void testProcess() throws IOException {

        String eventPlainText = IOUtils.resourceToString("/registry/hook_alpine-socat.json",
                StandardCharsets.UTF_8);
        EventDto event = new EventDecoder().process(eventPlainText);

        assertEquals(event.getAction(), "push");
        assertEquals(event.getRepository(), "alpine/socat");
        assertEquals(event.getTag(), "123");
        assertEquals(event.getIpAdress(), "127.0.0.1:54090");
        assertEquals(event.getUser(), null);
    }

}
