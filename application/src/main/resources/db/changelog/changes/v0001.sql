CREATE TABLE public."event" (
  id bigserial NOT NULL,
  "action" varchar(255) NOT NULL,
  created timestamp NOT NULL,
  ip_adress varchar(255) NOT NULL,
  repository varchar(255) NOT NULL,
  tag varchar(255) NOT NULL,
  username varchar(255) NOT NULL,
  CONSTRAINT event_pkey PRIMARY KEY (id)
);