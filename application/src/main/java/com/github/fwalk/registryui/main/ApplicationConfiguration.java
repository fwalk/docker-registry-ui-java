package com.github.fwalk.registryui.main;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApplicationConfiguration {

    private int eventsRetentionDays;

    private boolean registryAnyoneCanDelete;

    private String registryPassword;

    private String registryUrl;

    private String registryUsername;

    private boolean registryVerifyTls;

    private String[] uiAdmins;

    private String uiDefaultNamespace;

}
