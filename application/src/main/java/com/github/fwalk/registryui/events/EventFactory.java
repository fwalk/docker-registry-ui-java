package com.github.fwalk.registryui.events;

import java.util.Date;

public class EventFactory {

    /**
     * Creates an Event object with defaults and values from given DTO.
     * 
     * @param dto the DTO
     * @return Event
     */
    public static Event createEventWithDefaults(EventDto dto) {

        if (dto.getUser() == null) {
            dto.setUser("-");
        }

        if (dto.getTag() == null) {
            dto.setTag("latest");
        }

        if (dto.getCreated() == null) {
            dto.setCreated(new Date());
        }

        return new Event(dto.getAction(), dto.getRepository(), dto.getTag(), dto.getIpAdress(), dto.getUser(),
                        dto.getCreated());
    }

}
