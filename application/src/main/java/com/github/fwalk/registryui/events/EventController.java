package com.github.fwalk.registryui.events;

import com.github.fwalk.registryui.main.ApplicationConfiguration;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller()
public class EventController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ApplicationConfiguration appConfig;

    @Autowired
    EventHandler eventHandler;

    /**
     * Post handler for /api/events.
     * 
     * @param eventPlainText    Event as raw text
     * @return                  HTTP ok if processing successful
     * @throws UnsupportedEncodingException for failed processing
     */
    @PostMapping("/api/events")
    public ResponseEntity<String> receiveEvents(@RequestBody String eventPlainText)
            throws UnsupportedEncodingException {

        logger.info("Start processing: POST /api/events");
        eventHandler.parseAndStore(eventPlainText);
        return ResponseEntity.ok("Event successfully processed");
    }

    /**
     * Get handler for "/events".
     * 
     * @param model the model 
     * @return      view name
     */
    @GetMapping("/events")
    String viewEvents(Model model) {

        logger.debug("Start processing: GET /events");
        model.addAttribute("events", eventHandler.retrieveRecent(1000));
        return "event_log";
    }

}
