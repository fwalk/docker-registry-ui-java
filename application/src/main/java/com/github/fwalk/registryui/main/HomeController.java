package com.github.fwalk.registryui.main;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    String index() {

        return "forward:/ns/";
    }

}
