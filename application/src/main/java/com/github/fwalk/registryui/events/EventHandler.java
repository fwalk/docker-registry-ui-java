package com.github.fwalk.registryui.events;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    EventRepository eventRepository;

    /**
     * Parses the given Event raw-text and stores them into to the DB.  
     * @param eventPlainText    Event in raw-text
     */
    public void parseAndStore(String eventPlainText) {

        EventDto eventDto = new EventDecoder().process(eventPlainText);
        Event event = EventFactory.createEventWithDefaults(eventDto);
        Event event2 = eventRepository.save(event);
        logger.debug("New event added with id: {}, event: {}, src-event: {}", event2.getId(),
                event2, event);
    }

    public List<Event> retrieveAll() {

        return (List<Event>) eventRepository.findAll();
    }

    public List<Event> retrieveRecent(int limit) {

        return (List<Event>) eventRepository.findLastWithLimit(limit);
    }

    public List<Event> retrieveRepositoryRecent(String repository, int limit) {

        return (List<Event>) eventRepository.findByRepository(repository, limit);
    }

    public void cleanup(Date expiryDate) {
        eventRepository.deleteByCreatedBefore(expiryDate);
    }

}
