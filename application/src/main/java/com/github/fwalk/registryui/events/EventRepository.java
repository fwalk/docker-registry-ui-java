package com.github.fwalk.registryui.events;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

public interface EventRepository extends PagingAndSortingRepository<Event, Long> {

    default List<Event> findLastWithLimit(int limit) {
        return findAll(PageRequest.of(0, limit, Sort.by("id").descending())).getContent();
    }

    default List<Event> findByRepository(String repository, int limit) {
        return findByRepository(repository, PageRequest.of(0, limit, Sort.by("id").descending()));
    }

    List<Event> findByRepository(String repository, Pageable pageable);

    @Modifying
    @Transactional
    void deleteByCreatedBefore(Date expiryDate);

}
