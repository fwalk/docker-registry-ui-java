package com.github.fwalk.registryui.main;

import com.github.fwalk.registryui.events.EventHandler;
import com.github.fwalk.registryui.registry.Client;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ApiClient {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ApplicationConfiguration appConfig;

    @Autowired
    private Client client;

    @Autowired
    private EventHandler eventHandler;

    @PostConstruct
    private void postConstruct() {
        client.init();
    }

    /**
     * Gets a Client object.
     * 
     * @return  Client
     */
    public Client getClient() {
        if (!client.getInitErrors().isEmpty()) {
            throw new IllegalStateException(
                    "Cannot construct registry.Client, err:" + client.getInitErrors().toString());
        }
        return client;
    }

    /**
     * Gets a EventHandler object.
     * 
     * @return  EventHandler
     */
    public EventHandler getEventHandler() {
        return eventHandler;
    }

    /**
     * Cron task for count tags. 
     */
    @Scheduled(fixedRateString = "${cache_refresh_interval:100000}", initialDelay = 10000)
    public void countTags() {
        client.countTags();
    }

    /**
     * Cron task for prune image tags.
     */
    @Scheduled(fixedRateString = "${cache_refresh_interval:100000}", initialDelay = 50000)
    public void eventlogPrune() {
        Calendar cal = new GregorianCalendar();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_MONTH, -appConfig.getEventsRetentionDays());
        Date pruneDate = cal.getTime();
        logger.debug("Eventlog prune date is: {}, retentionDays: {}", pruneDate,
                appConfig.getEventsRetentionDays());

        eventHandler.cleanup(pruneDate);
    }

}
