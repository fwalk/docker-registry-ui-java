package com.github.fwalk.registryui.events;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.ReadContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventDecoder {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Converts Event in raw-text into an Event DTO.
     * @param eventPlainText    event in raw-text
     * @return                  Event DTO
     */
    public EventDto process(String eventPlainText) {

        Configuration config = Configuration.builder().options(Option.SUPPRESS_EXCEPTIONS).build();
        ReadContext jsonCtx = JsonPath.using(config).parse(eventPlainText);

        EventDto data = new EventDto();
        data.setAction(jsonCtx.read("$.events[0].action"));
        data.setRepository(jsonCtx.read("$.events[0].target.repository"));
        data.setTag(jsonCtx.read("$.events[0].target.tag"));
        data.setIpAdress(jsonCtx.read("$.events[0].request.addr"));
        data.setUser(jsonCtx.read("$.events[0].actor.name"));

        logger.debug("Parsed event data: {}", data);
        return data;
    }

}
