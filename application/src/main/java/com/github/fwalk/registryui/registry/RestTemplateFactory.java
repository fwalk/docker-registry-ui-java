package com.github.fwalk.registryui.registry;

import com.github.fwalk.registryui.main.ApplicationConfiguration;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateFactory {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ApplicationConfiguration appConfig;

    /**
     * Builds a Spring REST Template.
     * 
     * @return the REST Template
     * @throws KeyStoreException for failures
     * @throws NoSuchAlgorithmException for failures
     * @throws KeyManagementException for failures
     */
    public RestTemplate getRestTemplate()
                    throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

        // TODO: throw an application exception

        String username = appConfig.getRegistryUsername();
        String password = appConfig.getRegistryPassword();

        logger.trace("Add SSL without validation to RestTemplate");
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                        .loadTrustMaterial(null, acceptingTrustStrategy).build();
        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
        HttpComponentsClientHttpRequestFactory requestFactory =
                        new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        RestTemplate restTemplate = new RestTemplate(requestFactory);

        if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) {
            logger.trace("Add Basic-Authentication to RestTemplate");
            restTemplate.getInterceptors()
                            .add(new BasicAuthenticationInterceptor(username, password));
        }

        return restTemplate;
    }

}
