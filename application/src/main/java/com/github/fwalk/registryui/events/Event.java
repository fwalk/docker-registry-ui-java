package com.github.fwalk.registryui.events;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * Event entity class.
 * 
 * @author fwalk
 *
 */
@Entity
@Data
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String action;

    @NotNull
    private String repository;

    @NotNull
    private String tag;

    @NotNull
    @Column(name = "ip_adress") // FIXME
    private String ipAddress;

    @NotNull
    @Column(name = "username")
    private String user;

    @NotNull
    private Date created;

    /**
     * Prevent from instantiation.
     */
    protected Event() {}

    /**
     * Constructs an Event.
     * 
     * @param action action name
     * @param repository repository name
     * @param tag tag name
     * @param ipAddress the IP address
     * @param user user name
     * @param created created date
     */
    public Event(String action, String repository, String tag, String ipAddress, String user,
                    Date created) {
        this.action = action;
        this.repository = repository;
        this.tag = tag;
        this.ipAddress = ipAddress;
        this.user = user;
        this.created = created;
    }

}
