package com.github.fwalk.registryui.events;

import java.util.Date;

import lombok.Data;

@Data
public class EventDto {

    private String action;

    private String repository;

    private String tag;

    private String ipAdress;

    private String user;

    private Date created;

}
