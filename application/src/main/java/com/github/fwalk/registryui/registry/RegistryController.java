package com.github.fwalk.registryui.registry;

import com.github.fwalk.registryui.events.EventHandler;
import com.github.fwalk.registryui.main.ApiClient;
import com.github.fwalk.registryui.main.ApplicationConfiguration;
import com.github.fwalk.registryui.registry.Client.TagInfo;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/ns")
public class RegistryController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private ServletContext servletContext;

    @Autowired
    ApplicationConfiguration appConfig;

    @Autowired
    private ApiClient apiClient;

    @GetMapping("/")
    String handleIndex() {
        return "redirect:/ns/" + appConfig.getUiDefaultNamespace();
    }

    // TODO: cleanup exclusions, should be not needed anymore
    @GetMapping(value = "/{namespace:^.*(?![events|resources])}")
    String viewRepositories(Model model, @PathVariable String namespace,
                    @RequestParam("use-cache") Optional<Boolean> useCache) {

        logger.debug("Start processing: GET /ns/<namespace>, Namespace:{}, useCache:{}", namespace,
                        useCache.isPresent());
        
        model.addAttribute("contextpath", servletContext.getContextPath());
        model.addAttribute("namespace", namespace);
        
        Client registryClient = apiClient.getClient();
        Map<String, List<String>> catalog = registryClient.repositories(useCache.isPresent());
        List<String> nsList = new ArrayList<String>();
        nsList.add(namespace);
        for (String ns : registryClient.namespaces()) {
            if (!nsList.contains(ns)) {
                nsList.add(ns);
            }
        }

        if (catalog.isEmpty() || nsList.isEmpty()) {
            return "first_steps";
        }

        if (catalog.containsKey(namespace)) {
            model.addAttribute("namespaces", nsList);
            model.addAttribute("repositories", catalog.get(namespace));
            model.addAttribute("tagCounts", registryClient.tagCounts());

        } else {
            model.addAttribute("namespaces", nsList);
        }

        return "repositories";
    }

    @GetMapping(value = "/{namespace:^.*(?![events|resources])}/{repo}")
    String viewTags(Model model, @PathVariable String namespace, @PathVariable String repo,
                    @RequestHeader(value = "X-WEBAUTH-USER") Optional<String> userName)
                    throws UnsupportedEncodingException {

        logger.debug("Start processing: GET /ns/<namespace>/<repo>, Namespace:{}, repo:{}",
                        namespace, repo);

        Client registryClient = apiClient.getClient();
        String repoPath = repo;
        if (!namespace.contentEquals(appConfig.getUiDefaultNamespace())) {
            repoPath = String.format("%s/%s", namespace, repo);
        }
        List<String> tags = registryClient.tags(repoPath);
        model.addAttribute("namespace", namespace);
        model.addAttribute("repo", repo);
        model.addAttribute("tags", tags);
        model.addAttribute("repoPath", URLDecoder.decode(repoPath, "UTF-8"));
        model.addAttribute("deleteAllowed", registryClient.isDeleteAllowed(userName));

        EventHandler eventHandler = apiClient.getEventHandler();
        model.addAttribute("events", eventHandler.retrieveRepositoryRecent(repoPath, 50));

        return "tags";
    }

    @GetMapping(value = "/{namespace:^.*(?![events|resources])}/{repo}/{tag}")
    String viewTagInfo(Model model, @PathVariable String namespace, @PathVariable String repo,
                    @PathVariable String tag) throws URISyntaxException {

        logger.debug("Start processing: GET /ns/<namespace>/<repo>/<tag>, Namespace:{}, repo:{}, tag:{}",
                        namespace, repo, tag);

        Client registryClient = apiClient.getClient();
        String repoPath = repo;
        if (!namespace.contentEquals(appConfig.getUiDefaultNamespace())) {
            repoPath = String.format("%s/%s", namespace, repo);
        }
        TagInfo tagInfo = registryClient.tagInfo(repoPath, tag, false);

        model.addAttribute("registry_host", tagInfo.getRegistryHost());
        model.addAttribute("sha256", tagInfo.getSha256());
        model.addAttribute("details", tagInfo.getDetails());
        model.addAttribute("v1Layers", tagInfo.getDetails().getV1Layers());
        model.addAttribute("v2Layers", tagInfo.getDetails().getV2Layers());

        return "tag_info";
    }

    @GetMapping(value = "/{namespace:^.*(?![events|resources])}/{repo}/{tag}/delete")
    String deleteTag(Model model, @PathVariable String namespace, @PathVariable String repo,
                    @PathVariable String tag,
                    @RequestHeader(value = "X-WEBAUTH-USER") Optional<String> userName)
                    throws URISyntaxException {

        logger.debug("Start processing: GET /ns/<namespace>/<repo>/<tag>/delete, Namespace:{}, repo:{}, tag:{}",
                        namespace, repo, tag);

        Client registryClient = apiClient.getClient();
        String repoPath = repo;
        if (!namespace.contentEquals(appConfig.getUiDefaultNamespace())) {
            repoPath = String.format("%s/%s", namespace, repo);
        }

        if (registryClient.isDeleteAllowed(userName)) {
            registryClient.deleteTag(repoPath, tag);
        }

        return "redirect:/ns/" + namespace + "/" + repo;
    }

}
