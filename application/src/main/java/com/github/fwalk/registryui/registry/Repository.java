package com.github.fwalk.registryui.registry;

import java.util.List;

import lombok.Data;

@Data
public class Repository {

    private String name;

    private List<String> tags;
}
