package com.github.fwalk.registryui.registry;

import java.util.List;

import lombok.Data;

@Data
public class Catalog {

    public List<String> repositories;
}
