package com.github.fwalk.registryui;

import com.github.fwalk.registryui.main.ApplicationConfiguration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class Application {

    @Value("${eventlistener.retention_days}")
    private int eventsRetentionDays;

    @Value("${application.anyone_can_delete:false}")
    private boolean registryAnyoneCanDelete;

    @Value("${registry.password:null}")
    private String registryPassword;

    @Value("${registry.url:null}")
    private String registryUrl;

    @Value("${registry.username:null}")
    private String registryUsername;

    @Value("${registry.verify_tls:false}")
    private boolean registryVerifyTls;

    @Value("${application.admins}")
    private String[] uiAdmins;

    @Value("${application.default_namespace:library}")
    private String uiDefaultNamespace;

    /**
     * Build the application configuration.
     * 
     * @return application configuration
     */
    @Bean
    public ApplicationConfiguration registryConfiguration() {
        ApplicationConfiguration config = ApplicationConfiguration
                        .builder()
                        .eventsRetentionDays(eventsRetentionDays)
                        .registryAnyoneCanDelete(registryAnyoneCanDelete)
                        .registryPassword(registryPassword)
                        .registryUrl(registryUrl)
                        .registryUsername(registryUsername)
                        .registryVerifyTls(registryVerifyTls)
                        .uiAdmins(uiAdmins)
                        .uiDefaultNamespace(uiDefaultNamespace)
                        .build();
        return config;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
