package com.github.fwalk.registryui.registry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fwalk.registryui.main.ApplicationConfiguration;
import com.github.fwalk.registryui.main.Common;
import com.github.fwalk.registryui.main.ResourceNotFoundException;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Client implements the interaction with the docker registry.
 * 
 * @author fwalk
 *
 */
@Service
public class Client {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String registryUrl;

    @Autowired
    ApplicationConfiguration appConfig;

    private boolean anyoneCanDelete;

    private String[] admins;

    private String defaultNamespace;

    @Autowired
    private RestTemplateFactory restTemplateFactory;

    private String authUrl;

    private Map<String, List<String>> repos = new HashMap<String, List<String>>();

    private Map<String, Integer> tagsCounter = new HashMap<String, Integer>();

    public @Getter List<String> initErrors = new ArrayList<String>();


    /**
     * Initialize the Client, connects to the registry during this.
     */
    public void init() {

        registryUrl = appConfig.getRegistryUrl();
        anyoneCanDelete = appConfig.isRegistryAnyoneCanDelete();
        admins = appConfig.getUiAdmins();
        defaultNamespace = appConfig.getUiDefaultNamespace();

        logger.debug("Start init, registry-url: {}", registryUrl);

        RestTemplate restTemplate;
        try {
            restTemplate = restTemplateFactory.getRestTemplate();
        } catch (KeyManagementException | KeyStoreException | NoSuchAlgorithmException e) {
            addInitError(String.format(
                            "Failed during creation of rest-template, m: " + e.getMessage()));
            return;
        }

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(Common.httpHeaders());
        String url = registryUrl + "/v2/";
        ResponseEntity<String> respEntity =
                        restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);

        String authHeader = "";
        if (respEntity.getStatusCode().isError()) {
            addInitError(String.format("GET failed, url:%s", url));
            return;
        } else {
            if (respEntity.getStatusCode().is2xxSuccessful()) {
                logger.debug("Registry access successful, init done.");
                return;
            } else if (respEntity.getStatusCode().is4xxClientError()) {
                List<String> wwwAuthHeader = respEntity.getHeaders().get("WWW-Authenticate");
                if (wwwAuthHeader != null && !wwwAuthHeader.isEmpty()) {
                    authHeader = wwwAuthHeader.get(0);
                } else {
                    logger.debug("Unable to determine 'WWW-Authenticate' header attribute");
                }
            } else {
                logger.warn("Invalid result, statuscode: {}", respEntity.getStatusCode());
            }
        }

        logger.info("authHeader:{}", authHeader);
        if (authHeader.startsWith("Bearer")) {
            Pattern pattern = Pattern.compile("^Bearer realm=\"(http.+)\",service=\"(.+)\"");
            Matcher matcher = pattern.matcher(authHeader);
            if (matcher.matches()) {
                authUrl = String.format("%s?service=%s", matcher.group(1), matcher.group(2));
                logger.debug("Registry service is using Bearer token, url: {}, authURL: {}", url,
                                authUrl);
            }
            if (StringUtils.isEmpty(authUrl)) {
                throw new UnsupportedOperationException(
                                "Unable to find Bearer token details, url: " + url);
            }

        } else if (authHeader.toLowerCase().startsWith("basic")) {
            throw new UnsupportedOperationException(
                            "Not implemented this way, authHeader:" + authHeader);
        }

        logger.debug("Init done");
    }


    /**
     * Adds a message about the problem to a list.
     * 
     * @param msg the message
     */
    private void addInitError(String msg) {
        initErrors.add(msg);
        logger.error(msg);
    }

    /**
     * Checks the provided user name if delete action is allowed.
     * 
     * @param userName the user name
     * @return true if delete is allowed
     */
    public boolean isDeleteAllowed(Optional<String> userName) {
        boolean deleteAllowed = false;
        if (anyoneCanDelete) {
            deleteAllowed = true;
        } else if (userName.isPresent() && ArrayUtils.isNotEmpty(admins)) {
            deleteAllowed = ArrayUtils.contains(admins, userName.get());
        }
        return deleteAllowed;
    }

    @Data
    @AllArgsConstructor
    private class CallRegistryResponse {
        private JsonNode data;
        private String digest;
    }

    /**
     * Executes a call against the registry REST API.
     * 
     * <p>
     * Example:
     * 
     * <pre>
     * {@code
     * 
     * callRegistry("/v2/_catalog", "registry:catalog:*", 2, false)
     * 
     * }
     * </pre>
     * </p>
     * 
     * @param uri API path
     * @param scope scope in path
     * @param manifest requested manifest version (Mimetype)
     * @param delete flag to indicating a delete action
     * @return
     */
    public CallRegistryResponse callRegistry(String uri, String scope, int manifest,
                    boolean delete) {

        CallRegistryResponse crp = new CallRegistryResponse(null, null);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(Common.httpHeaders(manifest));
        RestTemplate restTemplate;
        String url = registryUrl + uri;
        try {
            restTemplate = restTemplateFactory.getRestTemplate();
            ResponseEntity<String> respEntity =
                            restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
            JsonNode data = new ObjectMapper().readTree(respEntity.getBody());
            crp.setData(data);
            crp.setDigest(respEntity.getHeaders().getFirst("Docker-Content-Digest"));
            if (!respEntity.getStatusCode().is2xxSuccessful()) {
                return crp;
            }
        } catch (KeyManagementException | KeyStoreException | NoSuchAlgorithmException
                        | IOException e1) {
            logger.warn("Registry-call with GET failed, url: {}, manifest: {}", url, manifest);
            return crp;
        }

        logger.debug("GET url: {}, size: {}", url, crp.getData().toString().length());

        if (delete) {
            url = registryUrl + StringUtils.substringBefore(uri, "/manifests/") + "/manifests/"
                            + crp.getDigest();
            try {
                ResponseEntity<String> respEntity = restTemplate.exchange(url, HttpMethod.DELETE,
                                httpEntity, String.class);
                if (respEntity.getStatusCode().is2xxSuccessful()) {
                    logger.info("DELETE {} ({}), statuscode: {}", uri,
                                    StringUtils.substringAfter(uri, "/manifests/"),
                                    respEntity.getStatusCodeValue());
                }
            } catch (RestClientException e) {
                logger.warn("Registry-call with DELETE failed, url: {}, manifest: {}", url,
                                manifest, e);
            }

            return crp;
        }

        return crp;
    }

    /**
     * Returns a sorted 'namespace' list, adds 'library' as default if not in the list.
     * 
     * @return the sorted 'namespace' list
     */
    public SortedSet<String> namespaces() {

        SortedSet<String> namespaces = new TreeSet<>();
        for (String k : repos.keySet()) {
            namespaces.add(k);
        }
        if (!namespaces.contains("library")) {
            namespaces.add("library");
        }
        return namespaces;
    }

    /**
     * Determines the content of the Docker registry.
     * 
     * @param useCache use the previous result as cache if true
     * @return a map with the repositories
     */
    public Map<String, List<String>> repositories(boolean useCache) {

        if (!repos.isEmpty() && useCache) {
            return repos;
        }

        synchronized (this) {
            CallRegistryResponse callReg =
                            callRegistry("/v2/_catalog", "registry:catalog:*", 2, false);
            if (callReg.data == null || callReg.data.get("repositories") == null) {
                logger.warn("Empty result from call-registry");
                return repos;
            }

            Catalog catalog = new ObjectMapper().convertValue(callReg.data, Catalog.class);
            logger.debug("Catalog: " + catalog.toString());

            for (String repoName : catalog.repositories) {
                String namespace = "library";
                if (repoName.contains("/")) {
                    namespace = StringUtils.substringBefore(repoName, "/");
                    repoName = StringUtils.substringAfter(repoName, "/");
                }
                if (!repos.containsKey(namespace)) {
                    repos.put(namespace, new ArrayList<String>());
                }
                List<String> repoNames = repos.get(namespace);
                if (!repoNames.contains(repoName)) {
                    repoNames.add(repoName);
                }
                repos.replace(namespace, repoNames);
            }
        }

        logger.debug("Repos: " + repos.toString());
        return repos;
    }

    /**
     * Delivers the 'tag' list for the provided repository.
     * 
     * @param repo repository name
     * @return 'tag' list
     */
    public List<String> tags(String repo) {

        String repositoryName = repo;
        if (repositoryName.startsWith(defaultNamespace)) {
            repositoryName = repositoryName.replaceFirst("^" + defaultNamespace + "/", "");
        }

        String uri = String.format("/v2/%s/tags/list", repositoryName);
        String scope = String.format("repository:%s:*", repositoryName);
        CallRegistryResponse callReg = callRegistry(uri, scope, 2, false);
        
        List<String> tagList = new ArrayList<String>();
        if (callReg.data == null || callReg.data.get("name") == null) {
            logger.warn("Empty result (data and name)");
            return tagList;
        }

        Repository repository = new ObjectMapper().convertValue(callReg.data, Repository.class);
        logger.debug("Repository: " + repository.toString());
        return (repository.getTags() != null) ? repository.getTags() : tagList;
    }

    @Data
    @Builder
    public static class TagInfo {
        private String registryHost;
        private String infoV1;
        private String infoV2;
        private String sha256;
        private ManifestDetail details;
    }

    /**
     * Determines the information for the provided image ('repo' + 'tag').
     * 
     * @param repo repository name
     * @param tag tag name
     * @param v1only 'true' if information is for v1 compatibility infomation
     * @return tagInfo
     * 
     * @throws URISyntaxException error if something happens
     */
    public TagInfo tagInfo(String repo, String tag, Boolean v1only) throws URISyntaxException {

        String uri = String.format("/v2/%s/manifests/%s", repo, tag);
        String scope = String.format("repository:%s:*", repo);

        CallRegistryResponse crV1 = callRegistry(uri, scope, 1, false);
        if (crV1.getData() == null) {
            return TagInfo.builder().build();
        }

        URI registryUri = new URI(registryUrl);
        String registryHost = String.format("%s:%s", registryUri.getHost(), registryUri.getPort());

        if (v1only) {
            return TagInfo.builder().registryHost(registryHost).infoV1(crV1.getData().toString())
                            .infoV2(null).sha256(null).build();
        }

        CallRegistryResponse crV2 = callRegistry(uri, scope, 2, false);
        if (crV2.getData() == null || crV2.getDigest() == null) {
            return TagInfo.builder().build();
        }

        TagInfo tagInfo = TagInfo.builder().registryHost(registryHost)
                        .infoV1(crV1.getData().toString()).infoV2(crV2.getData().toString())
                        .sha256(crV2.getDigest().substring(7)).build();

        tagInfo.setDetails(manifestDetails(tagInfo.getInfoV1(), tagInfo.getInfoV2()));

        return tagInfo;
    }

    @Data
    public class ManifestDetail {
        long imageSize;
        String tag;
        String repoPath;
        String created;
        int laversCount;
        List<Map<String, Object>> v1Layers;
        List<Map<String, Object>> v2Layers;
    }

    /**
     * Extracts the Manifest details from the provided v1 and v2 JSON fragments.
     * 
     * @param infoV1 JSON data v1
     * @param infoV2 JSOn data v2
     * 
     * @return manifest details
     */
    public ManifestDetail manifestDetails(String infoV1, String infoV2) {

        ManifestDetail details = new ManifestDetail();

        try {
            ReadContext ctxInfoV1 = JsonPath.parse(infoV1);
            details.setTag(ctxInfoV1.read("$.tag"));
            details.setRepoPath(ctxInfoV1.read("$.name"));
            String v1History0V1Compatibility = ctxInfoV1.read("$.history[0].v1Compatibility");
            details.setCreated(JsonPath.read(v1History0V1Compatibility, "$.created"));

            ReadContext ctxInfoV2 = JsonPath.parse(infoV2);
            if (ctxInfoV2.read("$.layers") != null) {
                List<Integer> layerSizes = ctxInfoV2.read("$.layers[*].size");
                details.setImageSize(layerSizes.stream().reduce(0, Integer::sum));
            } else {
                List<String> v2HistoryV1Compatibilities =
                                ctxInfoV2.read("$.history[*].v1Compatibility");
                for (String s : v2HistoryV1Compatibilities) {
                    details.setImageSize(details.getImageSize()
                                    + Integer.parseInt(JsonPath.read(s, "$.Size")));
                }
            }

            List<Map<String, Object>> v2Layers = ctxInfoV2.read("$.layers[*]");
            details.setV2Layers(v2Layers);
            details.setLaversCount(v2Layers.size());

            List<Map<String, Object>> v1Layers = new ArrayList<>();
            List<String> v1HistoryV1Compatibilities =
                            ctxInfoV1.read("$.history[*].v1Compatibility");
            for (String v1Compatibilities : v1HistoryV1Compatibilities) {
                Map<String, String> compatUnsorted = JsonPath.parse(v1Compatibilities).read("$");
                Map<String, Object> compatibilities = new TreeMap<String, Object>(compatUnsorted);
                for (Map.Entry<String, Object> c : compatibilities.entrySet()) {
                    String key = c.getKey();
                    Object value = JsonPath.parse(c.getValue()).read("$");
                    if (value instanceof Map) {
                        compatibilities.replace(key, value);
                    }
                }
                v1Layers.add(compatibilities);
            }
            details.setV1Layers(v1Layers);

        } catch (Exception e) {
            logger.warn("Determining manifest-info failed at least partially", e);
        }

        return details;
    }

    public Map<String, Integer> tagCounts() {
        return tagsCounter;
    }

    /**
     * Calculates the 'tags' in the registry. Typically this is performed with a cron task.
     */
    public void countTags() {

        logger.info("Calculating tags ...");
        for (Entry<String, List<String>> item : repositories(false).entrySet()) {
            String name = item.getKey();
            List<String> repos = item.getValue();
            for (String repo : repos) {
                String repoPath = repo;
                if (!name.startsWith(defaultNamespace)) {
                    repoPath = String.format("%s/%s", name, repo);
                }
                String tag = String.format("%s/%s", name, repo);
                int count = tags(repoPath).size();
                tagsCounter.put(tag, count);
            }
        }
        logger.info("Calculating tags complete");
    }

    /**
     * Performs a 'delete' action for the provided image (repo + tag).
     * 
     * @param repoPath image path
     * @param tag image tag
     */
    public void deleteTag(String repoPath, String tag) {

        logger.info("delete: repoPath:{}, tag:{}", repoPath, tag);
        String uri = String.format("/v2/%s/manifests/%s", repoPath, tag);
        String scope = String.format("repository:%s:*", repoPath);
        CallRegistryResponse callReg = callRegistry(uri, scope, 2, true);

        if (callReg.getData() == null) {
            throw new ResourceNotFoundException("Delete failed, uri: " + uri);
        }

    }

}
