package com.github.fwalk.registryui.main;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class Common {

    public static final MediaType MANIFEST_V1 =
                    new MediaType("application", "vnd.docker.distribution.manifest.v1+json");

    public static final MediaType MANIFEST_V2 =
                    new MediaType("application", "vnd.docker.distribution.manifest.v2+json");

    /**
     * Prevent from instantiation.
     */
    protected Common() {}

    /**
     * Pretty print the provided size.
     * 
     * @param size the size
     * @return size human readable
     */
    public static String prettySize(long size) {

        String[] units = {"B", "kB", "MB", "GB"};
        int idx = 0;
        while (size > 1024 && idx < units.length) {
            size = size / 1024;
            idx++;
        }

        return String.format("%s %s", size, units[idx]);
    }

    /**
     * Pretty print the provided date string.
     * 
     * @return date human readable
     */
    public static String prettyTime(String datetime) {

        String datetime2 = datetime.replace("T", " ");
        return StringUtils.substringBefore(datetime2, ".");
    }

    /**
     * Pretty print the provided date.
     * 
     * @return date human readable
     */
    public static String prettyTime(Date date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        return dateFormat.format(date);
    }

    /**
     * Gets the value of the given environment variable, return a default otherwise.
     * 
     * @param name environment variable name
     * @param value default value
     * @return variable value or default otherwise
     */
    public static String getEnvOrElse(String name, String value) {
        return (System.getenv(name) != null) ? System.getenv(name) : value;
    }

    /**
     * Build header attributes for the given manifest version.
     * 
     * @param manifest manifest version
     * @return header attributes
     */
    public static HttpHeaders httpHeaders(int manifest) {

        MediaType mediaType = (manifest == 1) ? MANIFEST_V1 : MANIFEST_V2;
        HttpHeaders httpHeaders = httpHeaders();
        httpHeaders.setAccept(Collections.singletonList(mediaType));

        return httpHeaders;
    }

    /**
     * Build header attributes with user-agent.
     * 
     * @return header attributes
     */
    public static HttpHeaders httpHeaders() {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("User-Agent", "docker-registry-ui");

        return httpHeaders;
    }

}
