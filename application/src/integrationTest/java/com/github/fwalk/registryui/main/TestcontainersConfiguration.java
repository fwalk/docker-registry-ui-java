package com.github.fwalk.registryui.main;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;

import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;

@Configuration
public class TestcontainersConfiguration {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static String db = Common.getEnvOrElse("POSTGRES_DB", "registry_ui");
    private static String dbUser = Common.getEnvOrElse("POSTGRES_USER", "registry_ui");
    private static String dbPassword = Common.getEnvOrElse("POSTGRES_PASSWORD", "password");

    @Autowired
    ApplicationConfiguration appConfig;

    @SuppressWarnings("rawtypes")
    private static PostgreSQLContainer dbContainer =
                    (PostgreSQLContainer) new PostgreSQLContainer("postgres").withDatabaseName(db)
                                    .withUsername(dbUser).withPassword(dbPassword)
                                    .withExposedPorts(5432);

    @SuppressWarnings("rawtypes")
    private static GenericContainer registryContainer =
                    new GenericContainer("registry").withExposedPorts(5000);

    /**
     * Constructs a datasource Bean with 'primary' priority.
     * 
     * @return the datasource Bean
     */
    @Bean
    @Primary
    public DataSource dataSource() {
        String datasourceUrl = String.format("jdbc:postgresql://%s:%s/registry_ui",
                        dbContainer.getContainerIpAddress(), dbContainer.getMappedPort(5432));
        final PGSimpleDataSource ds = new PGSimpleDataSource();
        ds.setUrl(datasourceUrl);
        ds.setUser(dbUser);
        ds.setPassword(dbPassword);
        return ds;
    }

    /**
     * Starts the prepared container and sets the registry URL property in application configuration.
     */
    @PostConstruct
    public void start() {
        logger.info("************************ CONTAINER start ************************");
        dbContainer.start();
        registryContainer.start();

        String registryUrl =
                        String.format("http://%s:%s", registryContainer.getContainerIpAddress(),
                                        registryContainer.getMappedPort(5000));
        appConfig.setRegistryUrl(registryUrl);
    }

    /**
     * Stops the assigned container.
     */
    @PreDestroy
    public void stop() {
        logger.info("************************ CONTAINER stop ************************");
        dbContainer.stop();
        registryContainer.stop();
    }

}
