package com.github.fwalk.registryui.main;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.github.fwalk.registryui.Application;
import com.github.fwalk.registryui.registry.Client;
import com.github.fwalk.registryui.registry.Client.ManifestDetail;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class RegistryClientTest {

    @Autowired
    private Client client;

    @Test
    public void initialisationWithoutErrors() {
        assertTrue("Init errors must not be empty.", client.getInitErrors().isEmpty());
    }

    @Test
    public void manifestInfoIsValid() throws IOException {

        String infoV1 = IOUtils.resourceToString("/registry/manifest01-infoV1.json",
                        StandardCharsets.UTF_8);
        assertNotNull("JSON string must not 'null'.", infoV1);
        String infoV2 = IOUtils.resourceToString("/registry/manifest01-infoV2.json",
                        StandardCharsets.UTF_8);
        assertNotNull("JSON string must not 'null'.", infoV2);

        ManifestDetail details = client.manifestDetails(infoV1, infoV2);
        assertEquals("Wrong image size.", details.getImageSize(), 308275877);
        assertTrue("Wrong image tag.", details.getTag().contentEquals("stretch"));
        assertTrue("Wrong repo-path.", details.getRepoPath().contentEquals("golang"));
        assertTrue("Wrong image size.", details.getCreated().contains("2019-02-06T12:53:36"));

        assertEquals("Wrong number of layers determied.", details.getLaversCount(), 7);
        assertEquals("Wrong layer size.", (int) details.getV2Layers().get(3).get("size"), 50063121);
    }
}
