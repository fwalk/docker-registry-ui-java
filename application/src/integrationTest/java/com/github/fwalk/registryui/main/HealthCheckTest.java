package com.github.fwalk.registryui.main;

import static io.restassured.RestAssured.given;

import com.github.fwalk.registryui.Application;

import io.restassured.RestAssured;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class HealthCheckTest {

    @Autowired
    ApplicationConfiguration appConfig;

    @LocalServerPort
    int port;

    String contextPath = "";

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    public void requestHealthCheck_checkResult_expectSuccess() {

        given().basePath(contextPath).when().get("/manage/health").then().statusCode(200);
        // TODO: pmd check reports: JUnit tests should include assert() or fail()
    }
}
