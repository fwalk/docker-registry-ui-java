#!/bin/bash

export CURRENT_UID=$(id -u):$(id -g) 

. ./stop.sh

docker-compose build --no-cache

docker-compose up -d

docker-compose logs -f