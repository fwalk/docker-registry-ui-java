FROM gradle:5.2-jdk as builder

ENV SRC=https://gitlab.com/fwalk/docker-registry-ui-java.git

RUN git clone --depth=1 ${SRC} app && \
cd app && \
gradle bootjar && \
cp -vf application/build/libs/application*.jar build/libs/app.jar

FROM openjdk:11-jre-slim
EXPOSE 8000
COPY --from=builder /home/gradle/app/build/libs/app.jar /app.jar
WORKDIR /app
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
