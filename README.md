
# Docker Registry UI

This is a Java implementation of the Golang version developed by [github.com/Quiq](https://github.com/Quiq/docker-registry-ui). 

Intension of this project was to understand differences between Golang and Spring. Only a subset of the features has been implemented.
Additionally there are some variants used to build Docker-Images.

![screenshot](./.img/docker-registry-ui-java.png)

## Configuration

Default configuration is located in [application.yaml](application/src/main/resources/application.yaml). 
These can be overwritten with environment variables accordingly to [Spring convention](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html) for externalizing configuration.

| Property                      | Env.-Variable                 | Example-Value        |
|-------------------------------|-------------------------------|----------------------|
| registry.url                  | REGISTRY_URL                  | http://registry:5000 |
| registry.username             | REGISTRY_USERNAME             | user                 |
| registry.password             | REGISTRY_PASSWORD             | password             |
| application.anyone_can_delete | APPLICATION_ANYONE_CAN_DELETE | false                |
| application.admins            | APPLICATION_ADMINS            | user                 | 
| db.server.host                | DB_SERVER_HOST                | localhost            |
| db.server.port                | DB_SERVER_PORT                | 5432                 |     
| db.dbname                     | Db_DBNAME                     | registry_ui          |
| db.username                   | DB_USERNAME                   | registry_ui          |
| db.password                   | DB_PASSWORD                   | password             |

## Build / Run

```plain
./gradlew bootRun
```

## Apply tag

Select appropriate branch: e.g. "0.2.x"

Check preconditions: 

- Git tag already set 
- Set manually (https://github.com/nebula-plugins/nebula-release-plugin#caveats)

Run:

```plain
./gradlew final
```

## Docker

### Run with host-network

```plain
docker run --name registry_ui -d --rm --network host registry_ui
```

### Build image

#### Gradle with JIB

```plain
./gradlew jibDockerBuild
```

Without Docker:

```plain
./gradlew jib
```

Override image-name:

```plain
--image=<image-ref-spec>
```

#### Gradle via S2I (Source-to-Image)

```plain
./gradlew docker:s2i:assemble
```

#### Docker deployment/Dockerfile

Build from Git source.

```plain
docker build -t registry_ui .
```

#### Docker dev-dependencies deployment/dev-local/docker-compose.yml

```plain
docker-compose build
docker-compose up -d
```

### Non-persistent profile

A non-persistent profile can be activated with "-Dspring.profiles.active=default,nonpersistent".

## Author information

fwalk___gitlab

## References

- https://github.com/Quiq/docker-registry-ui
- https://github.com/openshift/source-to-image
- https://docs.gradle.org/current/userguide/userguide.html
- https://github.com/GoogleContainerTools/jib
- https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html
- https://auth0.com/blog/integrating-spring-data-jpa-postgresql-liquibase/
- https://github.com/mladenbolic/static-code-analysis.git
- https://github.com/google/styleguide , https://google.github.io/styleguide/javaguide.html

